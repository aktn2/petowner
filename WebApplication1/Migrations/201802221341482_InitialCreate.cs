namespace WebApplication1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Pets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 6),
                        Type = c.String(nullable: false, maxLength: 100),
                        Breed = c.String(nullable: false, maxLength: 100),
                        DOB = c.String(nullable: false, maxLength: 50),
                        MedicalNotes = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Pets");
        }
    }
}

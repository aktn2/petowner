﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace WebApplication1.Models
{
    public class Pet
    {
        public Pet()
        {

        }

        [Key]
        [Required]
        public int ID { get; set; }

        [Required(ErrorMessage = "Pet name is required")]
        [Display(Name = "Name")]
        [StringLength(6, MinimumLength = 2)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Pet type is required")]
        [StringLength(100, MinimumLength = 2)]
        public string Type { get; set; }

        [Required(ErrorMessage = "Breed is required")]
        [StringLength(100, MinimumLength = 2)]
        public string Breed { get; set; }

        [Required(ErrorMessage = "Date of Birth is required")]
        [StringLength(50, MinimumLength = 2)]
        public string DOB { get; set; }

        [StringLength(200, MinimumLength = 2)]
        public string MedicalNotes { get; set; }
    }

    

    public class PetDBContext : DbContext
    {
        public DbSet<Pet> PetList
        {
            get; set;
        }
    }
}
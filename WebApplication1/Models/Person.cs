﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Models
{
    public class Person
    {
        [Key]
        [Required]
        public int ID { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [Display(Name = "Title")]
        [StringLength(6, MinimumLength = 2)]
        public string Title { get; set; }

        [Required(ErrorMessage = "First Name is required")]
        [StringLength(100, MinimumLength = 2)]
        public string ForeName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        [StringLength(100, MinimumLength = 2)]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [StringLength(100, MinimumLength = 2)]
        public string Address { get; set; }

        [Required(ErrorMessage = "Phone is required")]
        [StringLength(100, MinimumLength = 2)]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Date of Birth is required")]
        [StringLength(100, MinimumLength = 2)]
        public string DOB { get; set; }

        public string Avatar { get; set; }

       

        public Person() { }
    }

    public class PersonList
    {
        public List<Person> personList { get; set; }

        public PersonList()
        {
           
        }

        

        

      
       
    }

    
    public class PersonDBContext: DbContext
    {
        public DbSet<Person> PersonList
        {
            get; set;
        }
    }

        

   
}
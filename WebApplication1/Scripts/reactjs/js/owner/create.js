﻿
import React from 'react'
import ReactDOM from 'react-dom'

class ContactItem extends React.Component {
    render() {
        return (
            React.createElement('li', { className: 'ContactItem' },
                React.createElement('h2', { className: 'ContactItem-title' }, this.props.title),
                React.createElement('a', { className: 'ContactItem-forename' }, this.props.foreName),
                React.createElement('div', { className: 'ContactItem-lastname' }, this.props.lastName)
            )
        );
    }
}


class ContactForm extends React.Component {

    constructor(props) {
        super(props);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.onFirstNameChange = this.onFirstNameChange.bind(this);
        this.onLastNameChange = this.onLastNameChange.bind(this);
        this.onMobileChange = this.onMobileChange.bind(this);
        this.onAddressChange = this.onAddressChange.bind(this);
        this.onDOBChange = this.onDOBChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onTitleChange(e) {
        this.props.onChange(Object.assign({}, this.props.value, { title: e.target.value }));
        console.log(e.target.value);
    }

    onFirstNameChange(e) {
        this.props.onChange(Object.assign({}, this.props.value, { foreName: e.target.value }));
    }

    onLastNameChange(e) {
        this.props.onChange(Object.assign({}, this.props.value, { lastName: e.target.value }));
    }

    onMobileChange(e) {
        this.props.onChange(Object.assign({}, this.props.value, { mobile: e.target.value }));
    }

    onAddressChange(e) {
        this.props.onChange(Object.assign({}, this.props.value, { address: e.target.value }));
    }

    onDOBChange(e) {
        this.props.onChange(Object.assign({}, this.props.value, { DOB: e.target.value }));
    }

    onSubmit(e) {
        
        e.preventDefault();
        console.log(JSON.stringify(this.state, null, 4))
        this.props.onSubmit();
        
    }

    render() {
        var oldContact = this.props.value;
        var onChange = this.props.onChange;

        return (
            React.createElement('form', { className: 'ContactForm' },
                React.createElement('input', {
                    type: 'text',
                    placeholder: 'Title',
                    value: this.props.value.title,
                    onChange: this.onTitleChange,
                }),
                React.createElement('input', {
                    type: 'text',
                    placeholder: 'First Name',
                    value: this.props.value.foreName,
                    onChange: this.onFirstNameChange,
                }),
                React.createElement('input', {
                    type: 'text',
                    placeholder: 'Last Name',
                    value: this.props.value.lastName,
                    onChange: this.onLastNameChange,
                }),
                React.createElement('input', {
                    type: 'text',
                    placeholder: 'Mobile',
                    value: this.props.value.mobile,
                    onChange: this.onMobileChange,
                }),
                React.createElement('input', {
                    type: 'text',
                    placeholder: 'Address',
                    value: this.props.value.address,
                    onChange: this.onAddressChange,
                }),
                React.createElement('input', {
                    type: 'text',
                    placeholder: 'Date of Birth',
                    value: this.props.value.DOB,
                    onChange: this.onDOBChange,
                }),
                React.createElement('button', { type: 'submit' }, "Add")
            )
        );
    }
}

class ContactView extends React.Component {
    render() {
        var contactItemElements = this.props.contacts
            .filter(function (contact) {
                return contact.foreName;
            })
            .map(function (contact) {
                return React.createElement(ContactItem, contact);
            });

        return (
            React.createElement('div', { className: 'ContactView' },
                React.createElement('h1', { className: 'ContactView-title' }, "Contacts"),
                React.createElement('ul', { className: 'ContactView-list' }, contactItemElements),
                React.createElement(ContactForm, {
                    value: this.props.newContact,
                    onChange: this.props.onNewContactChange,
                    onSubmit: this.props.onNewContactSubmit,
                })
            )
        );
    }
}

var CONTACT_TEMPLATE = { title: "", foreName: "", lastName: "", mobile: "", address: "", DOB: "", avatar: "", errors: null };




function updateNewContact(contact) {
    setState({ newContact: contact });
}


function submitNewContact() {
    var contact = Object.assign({}, state.newContact, { key: state.contacts.length + 1, errors: {} });

    if (contact.foreName && contact.lastName) {
        setState(
            Object.keys(contact.errors).length === 0
                ? {
                    newContact: Object.assign({}, CONTACT_TEMPLATE),
                    contacts: state.contacts.slice(0).concat(contact),
                }
                : { newContact: contact }
        );
    }
}


var state = {};

function setState(changes) {
    Object.assign(state, changes);

    ReactDOM.render(
        React.createElement(ContactView, Object.assign({}, state, {
            onNewContactChange: updateNewContact,
            onNewContactSubmit: submitNewContact,
        })),
        document.getElementById('form')
    );
}

setState({
    contacts: [
        { key: 1, title: "Mr", foreName: "Michael", lastName: "hands", mobile: "4324211324", address: "101 Fake Street", DOB: "1/11/2011", avatar: "http://placekitten.com/g/64/64" },
        { key: 2, title: "Mrs", foreName: "Wat", lastName: "hands", mobile: "432442", address: "Marco Rd", DOB: "22/2/2011", avatar: "http://placekitten.com/g/64/64" }
    ],
    newContact: Object.assign({}, CONTACT_TEMPLATE),
});

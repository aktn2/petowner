﻿import React from 'react'
import ReactDOM from 'react-dom'

class PetForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'Nyo',
            type: 'Cat',
            breed: 'Normal cat',
            DOB: 'unknown',
            medicalNotes: 'pregnant twice'
        };

       
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleTypeChange = this.handleTypeChange.bind(this);
        this.handleBreedChange = this.handleBreedChange.bind(this);
        this.handleDOBChange = this.handleDOBChange.bind(this);
        this.handleMedicalNotesChange = this.handleMedicalNotesChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        this.setState({ name: event.target.value });
    }

    handleTypeChange(event) {
        this.setState({ type: event.target.value });
    }

    handleBreedChange(event) {
        this.setState({ breed: event.target.value });
    }

    handleDOBChange(event) {
        this.setState({ DOB: event.target.value });
    }

    handleMedicalNotesChange(event) {
        this.setState({ medicalNotes: event.target.value });
    }

    handleSubmit(event) {
        alert(this.state.name + this.state.type + this.state.breed + this.state.DOB + this.state.medicalNotes);
        event.preventDefault();
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Pet's Name
                    <input type="text" value={this.state.name} onChange={this.handleNameChange} />
                </label>
                <br />
                <label>
                    Pet Type:
                    <select value={this.state.type} onChange={this.handleTypeChange}>
                        <option value="cat">Cat</option>
                        <option value="dog">Dog</option>
                        <option value="monkey">Monkey</option>
                    </select>
                </label>
                <br />
                <label>
                    Breed
                    <input type="text" value={this.state.breed} onChange={this.handleBreedChange} />
                </label>
                <br />
                <label>
                    Date of Brith
                    <input type="text" value={this.state.DOB} onChange={this.handleDOBChange} />
                </label>
                <br />
                <label>
                    Medical Notes
                    <textarea value={this.state.medicalNotes} onChange={this.handleMedicalNotesChange} />
                </label>
                <br />
                <input type="submit" value="Submit" />
            </form>
        );
    }
}


ReactDOM.render(
    <PetForm />,
    document.getElementById('form')
);
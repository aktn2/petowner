﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class PersonController : Controller
    {
        public PersonDBContext _dbContext;

       

        // GET: Form
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Store(Person person)
        {
  
            return Json(person);
        }

        public ActionResult Edit(int id = 0)
        {
            var person = _dbContext.PersonList.First(x => x.ID == id);
            return View(person);
        }

        [HttpDelete]
        public ActionResult Delete(int id)
        {
            return RedirectToAction("index");
        }

      
    }
}